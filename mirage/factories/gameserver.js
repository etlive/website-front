import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() { return `ET ${faker.internet.domainWord()}`; },
  mod: faker.list.random('legacy', 'jaymod', 'etmain', 'etpro'),
  address() { return faker.internet.ip(); }
});
