import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.get('store').findAll('gameserver');
  },

  setupController(controller, model) {
    controller.set('gameservers', model);
  }
});
