import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
  gameservers: null,
  searchValue: '',
  modFilter: '',

  internalGameservers: computed('gameservers.[]', 'searchValue', 'modFilter', function() {
    const searchValue = this.get('searchValue');
    const modFilter = this.get('modFilter');

    return this.get('gameservers').filter(gameserver => {
      let search = true;
      let filter = true;

      if(!Ember.isEmpty(searchValue)) {
        search = gameserver.get('name').indexOf(searchValue) !== -1;
      }

      if(!Ember.isEmpty(modFilter)) {
        filter = gameserver.get('mod') === modFilter;
      }

      return filter && search;
    });
  })
});
