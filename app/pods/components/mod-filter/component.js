import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
  gameservers: null,
  selected: '',

  actions: {
    didSelectFilter(value) {
      this.set('selected', value);
      this.sendAction('update', value);
    }
  },

  _mods: computed('gameservers', function() {
    return this.get('gameservers').mapBy('mod').uniq();
  })
});
