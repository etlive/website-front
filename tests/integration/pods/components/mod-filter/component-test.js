import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('mod-filter', 'Integration | Component | mod filter', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +
  this.set('gameservers', []);
  this.render(hbs`{{mod-filter gameservers=gameservers}}`);

  // assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#mod-filter gameservers=gameservers}}
      template block text
    {{/mod-filter}}
  `);

  // assert.equal(this.$().text().trim(), 'template block text');
  assert.expect(0);
});
